package org.remote.server;

import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created with IntelliJ IDEA.
 * User: marco
 * Date: 4/25/13
 * Time: 11:16 AM
 * Part of the RemoteCommunication-Server-Project.
 */

public class Main {

    public static void main(String[] args) {
        try {
            String message = "";
            do {
                System.out.println("Warte auf Verbindung auf Port "+Constants.PORT);
                final ServerSocket serverSocket = new ServerSocket(Constants.PORT);
                final Socket clientSocket = serverSocket.accept();
                System.out.println("Verbindung hergestellt! Warte auf Befehle...");
                final InputStream inputStream = clientSocket.getInputStream();
                int c;
                message = "";
                while((c = inputStream.read()) != -1){
                    message += (char)c;
                }
                System.out.println("Befehl: "+message);
                inputStream.close();
                clientSocket.close();
                serverSocket.close();
            } while(!message.equals("ende"));
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }
}
